import React from "react";
import { isEqual } from "../helper";

interface IProps {
  languageList: string[];
}

export class Header extends React.Component<IProps> {
  public shouldComponentUpdate(nextProps: IProps) {
    if (isEqual(nextProps.languageList, this.props.languageList)) {
      return false;
    }
    return true;
  }

  public render() {
    const languageHeaderList = this.props.languageList.map(language => (
      <th key={language}>{language}</th>
    ));

    return (
      <thead>
        <tr>
          <th>Key</th>
          {languageHeaderList}
        </tr>
      </thead>
    );
  }
}
