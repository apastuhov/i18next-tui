import React from "react";
import classNames from "./../util/classnames";

interface IProps {
  value: string;
  empty: boolean;
}

export class Cell extends React.PureComponent<IProps> {
  public render() {
    const className = classNames({
      empty: this.props.empty,
    });

    return (
      <td className={className}>{this.props.empty ? "" : this.props.value}</td>
    );
  }
}
