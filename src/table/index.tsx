import React from "react";
import classNames from "./../util/classnames";
import { Header } from "./header";
import { Row } from "./row";

interface IProps {
  localeList: I18NextTUI.ILocale[];
  changed: Set<string>;
  filterEmpty: boolean;
  selectedKey: string;
  onSelect: (key: string) => void;
  defaultEmptyPlaceholder?: string;
  masterLanguage: string;
}

export class Table extends React.Component<IProps> {
  public render() {
    const languageList = this.props.localeList.map(v => v.lang);
    const className = classNames({
      "filter-empty": this.props.filterEmpty,
    });
    return (
      <div className="i18next-tui-table">
        <table className={className}>
          <Header languageList={languageList} />
          {this.getTableBody()}
        </table>
      </div>
    );
  }

  private getTableBody() {
    let master = this.props.localeList.find(v => v.lang === this.props.masterLanguage);

    if (!master) {
      master = this.props.localeList[0];
    }

    const result = [];

    for (const key in master.translations) {
      if (master.translations.hasOwnProperty(key)) {
        result.push(this.buildRow(key));
      }
    }

    return <tbody>{result}</tbody>;
  }

  private buildRow(key: string) {
    const translationMap: {[language: string]: string} = {};
    this.props.localeList.forEach(locale => {
      translationMap[locale.lang] = locale.translations[key];
    });
    return (
      <Row
        {...translationMap}
        changed={this.props.changed.has(key)}
        current={key === this.props.selectedKey}
        defaultEmptyPlaceholder={this.props.defaultEmptyPlaceholder}
        key={key}
        translationKey={key}
        onSelect={this.props.onSelect}
      />
    );
  }
}
