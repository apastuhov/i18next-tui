import React, { MouseEvent } from "react";
import { isEmpty } from "../helper";
import classNames from "./../util/classnames";
import { Cell } from "./cell";

interface IProps {
  translationKey: string;
  onSelect: (key: string) => void;
  defaultEmptyPlaceholder?: string;
  changed: boolean;
  current: boolean;
  [language: string]: string | boolean | ((key: string) => void) | undefined;
}

export class Row extends React.PureComponent<IProps> {
  public render() {
    const {
      translationKey,
      onSelect,
      defaultEmptyPlaceholder,
      changed,
      current,
      ...translationMap } = this.props;
    const translationCellList = [];
    let hasEmpty = false;

    for (const language in translationMap) {
      if (translationMap.hasOwnProperty(language)) {
        const translationValue = (translationMap[language] || "").toString();

        const empty = isEmpty(translationValue, defaultEmptyPlaceholder);
        if (empty) {
          hasEmpty = true;
        }

        translationCellList.push(
          <Cell key={language} empty={empty} value={translationValue} />,
        );
      }
    }

    const className = classNames({
      changed,
      current,
      empty: hasEmpty,
    });

    return (
      <tr onClick={this.handleCLick} className={className}>
        <td>{translationKey}</td>
        {translationCellList}
      </tr>
    );
  }

  private handleCLick = (event: MouseEvent<HTMLTableRowElement>) => {
    event.preventDefault();
    this.props.onSelect(this.props.translationKey);
  }
}
