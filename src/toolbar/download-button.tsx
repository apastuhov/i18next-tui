import React, { MouseEvent } from "react";

interface IProps {
  value: string;
  onClick: (value: string) => void;
}

export class DownloadButton extends React.PureComponent<IProps> {
  public render() {
    return (
      <button className="default" onClick={this.handleDownload}>
        {this.props.value}
      </button>
    );
  }

  private handleDownload = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    this.props.onClick(this.props.value);
  }
}
