import React from "react";

interface IProps extends React.Props<{}> {
  label: string;
}

export function ToolbarItem(props: IProps) {
  return (
    <div className="toolbar-item">
      {props.label}:
      {props.children}
    </div>
  );
}
