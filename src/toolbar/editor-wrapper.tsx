import React, { ChangeEvent } from "react";

interface IProps {
  languageList: string[];
  currentLanguage: string;
  onChangeLanguage: (language: string) => void;
}

export class EditorWrapper extends React.Component<IProps> {
  public render() {
    const optionList = this.props.languageList.map(lang => (
      <option key={lang} value={lang}>
        {lang}
      </option>
    ));

    return (
      <div className="i18next-tui-editor">
        <div className="row">
          <div className="title">
            Translation:
            <select
              defaultValue={this.props.currentLanguage}
              onChange={this.handleChangeLang}
            >
              {optionList}
            </select>
          </div>
          <div className="box">{this.props.children}</div>
        </div>
      </div>
    );
  }

  private handleChangeLang = (event: ChangeEvent<HTMLSelectElement>) => {
    event.preventDefault();
    this.props.onChangeLanguage(event.target.value);
  }
}
