import React, { ChangeEvent } from "react";

interface IProps {
  translationKey: string;
  value: string;
  onChange: (key: string, newValue: string) => void;
}

export class Editor extends React.PureComponent<IProps> {
  public render() {
    return (
      <textarea
        name={this.props.translationKey}
        value={this.props.value}
        onChange={this.handleChange}
      />
    );
  }

  private handleChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
    event.preventDefault();
    this.props.onChange(
      this.props.translationKey,
      event.target.value,
    );
  }
}
