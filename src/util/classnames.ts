interface IClassNamesHash {
  [key: string]: boolean;
}

interface IClassNamesArray extends Array<any> {
  [index: number]: string;
}

type ClassNames = IClassNamesHash | IClassNamesArray;

export default function classnames(classNames: ClassNames = []) {
  const classNameList = Array.isArray(classNames)
    ? classNames.filter(Boolean)
    : Object.keys(classNames).filter(className => classNames[className]);

  return classNameList.join(" ");
}
