import React from "react";
import "./index.sass";
import { Translator } from "./translator";

export function I18NextTUI(props: I18NextTUI.IProps) {
  return <Translator {...props} />;
}
