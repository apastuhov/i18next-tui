import React, { ChangeEvent } from "react";
import { service } from "./service";
import { Table } from "./table";
import { DownloadButtonList } from "./toolbar/download-button-list";
import { Editor } from "./toolbar/editor";
import { EditorWrapper } from "./toolbar/editor-wrapper";
import { NamespaceSwitch } from "./toolbar/namespace-switch";
import { ToolbarItem } from "./toolbar/toolbar-item";

export interface IINternalNamespace {
  name: string;
  locales: I18NextTUI.ILocale[];
  changed: Set<string>;
}

export interface IState {
  namespace: IINternalNamespace | null;
  selectedRowKey: string;
  filterEmpty: boolean;
  editorLanguage: string;
}

export class Translator extends React.Component<I18NextTUI.IProps, IState> {
  public state: IState = {
    editorLanguage: "en",
    filterEmpty: false,
    namespace: null,
    selectedRowKey: "",
  };

  public componentDidMount() {
    if (this.props.namespaceList && this.props.namespaceList.length > 0) {
      this.setNamespace(this.props.namespaceList[0]);
    }

    service.preventWidnowReload();
  }

  public render() {
    const namespace = this.state.namespace;
    if (!namespace) {
      return "...loading or missed namespace";
    }
    const availableLanguageList = namespace.locales.map(v => v.lang);
    const availableNamespaceList = this.props.namespaceList.map(v => v.name);
    const value = service.getTranslationByKey(namespace, this.state.editorLanguage, this.state.selectedRowKey);

    return (
      <div className="i18next-tui-tool">
        <div className="i18next-tui-toolbar">
          <NamespaceSwitch
            current={namespace.name}
            list={availableNamespaceList}
            onChange={this.handleChangeNamespace}
          />
          <DownloadButtonList valueList={availableLanguageList} onDownload={this.handleDownload} />

          <ToolbarItem label="Display empty">
            <input
              checked={this.state.filterEmpty}
              className="filter-empty"
              type="checkbox"
              onChange={this.handleDisplayEmpty}
            />
          </ToolbarItem>
        </div>
        <Table
          localeList={namespace.locales}
          changed={namespace.changed}
          filterEmpty={this.state.filterEmpty}
          selectedKey={this.state.selectedRowKey}
          onSelect={this.handleRowSelect}
          defaultEmptyPlaceholder={this.props.defaultEmptyPlaceholder}
          masterLanguage={"en"}
        />

        <EditorWrapper
          languageList={availableLanguageList}
          currentLanguage={this.state.editorLanguage}
          onChangeLanguage={this.handleChangeLanguage}
        >
          <Editor
            translationKey={this.state.selectedRowKey}
            value={value}
            onChange={this.handleTranslationChange}
          />
        </EditorWrapper>
      </div>
    );
  }

  private handleChangeLanguage = (editorLanguage: string) => {
    this.setState({
      editorLanguage,
    });
  }

  private handleRowSelect = (key: string) => {
    if (this.state.selectedRowKey !== key) {
      this.setState({
        selectedRowKey: key,
      });
    }
  }

  private handleTranslationChange = (
    key: string,
    translation: string,
  ) => {
    const namespace = this.namespace;
    const translations = service.getTranslations(namespace, this.state.editorLanguage);

    translations[key] = translation;

    if (!namespace.changed) {
      namespace.changed = new Set<string>();
    }

    namespace.changed.add(key);

    this.setState({
      namespace,
    });
  }

  private handleChangeNamespace = (name: string) => {
    this.setNamespace(name);
  }

  private handleDisplayEmpty = (event: ChangeEvent<HTMLInputElement>) => {
    this.setState({
      filterEmpty: event.target.checked,
      selectedRowKey: "",
    });
  }

  private handleDownload = (lang: string) => {
    service.downloadJson(this.namespace, lang);
  }

  private setNamespace(namespace: I18NextTUI.INamespace | string) {
    if (typeof namespace === "string") {
      const foundItem = this.props.namespaceList.find(
        v => v.name === namespace,
      );
      if (!foundItem) {
        throw new Error("No such namespace");
      }
      namespace = foundItem;
    }

    if (!namespace.hasOwnProperty("changes")) {
      namespace.changed = new Set();
    }

    let selectedRowKey = "";
    const translations = service.getTranslations(namespace, this.state.editorLanguage);
    const keys = Object.keys(translations);
    if (keys.length > 0) {
      selectedRowKey = keys[0];
    }

    this.setState({
      namespace: namespace as IINternalNamespace,
      selectedRowKey,
    });
  }

  private get namespace() {
    const namespace = this.state.namespace;
    if (!namespace) {
      throw new Error("You should have at least any namespace selected");
    }
    return namespace;
  }
}
