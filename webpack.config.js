const path = require("path");
const webpack = require("webpack");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

const isProductionMode = process.argv.includes("--production");
const isAnalyze = process.argv.includes("--analyze");
const mode = isProductionMode ? "production" : "development";

const minimizationPluggins = [
  new CompressionPlugin({
    algorithm: "gzip",
    asset: "[path].gz[query]",
    minRatio: 0.8,
    test: /\.js$/,
    threshold: 10240,
  }),
];

module.exports = {
  mode,
  target: "web",
  devServer: {
    contentBase: path.resolve("."),
    before(app) {
      /**
       * Redirect on example page
       */
      app.get("/", function(req, res) {
        res.redirect("/demo/");
      });
      /**
       * Use hot reload module for examples
       */
      app.get("/demo/dist/i18next-tui.js", function(req, res) {
        res.redirect("/i18next-tui.js");
      });
      app.get("/demo/node_modules/react/umd/react.development.js", function(req, res) {
        res.redirect("/node_modules/react/umd/react.development.js");
      });
      app.get("/demo/node_modules/react-dom/umd/react-dom.development.js", function(req, res) {
        res.redirect("/node_modules/react-dom/umd/react-dom.development.js");
      });
    },
  },
  entry: "./src/index.tsx",
  output: {
    libraryTarget: "umd",
    library: "I18NextTUI",
    filename: "i18next-tui.js",
    path: path.resolve(__dirname, "dist"),
  },
  devtool: isProductionMode ? undefined : "cheap-eval-source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json", ".sass"],
    plugins: [new TsconfigPathsPlugin({ configFile: "tsconfig.json" })],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /(node_modules|__tests__)/,
        loader: "ts-loader",
      },
      isProductionMode
        ? {}
        : {
            enforce: "pre",
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "source-map-loader",
          },
      {
        test: /\.sass$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          { loader: "sass-loader" },
        ],
      },
    ],
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    isAnalyze ? new BundleAnalyzerPlugin() : () => {},
    ...(isProductionMode ? minimizationPluggins : []),
  ],
  externals: [
    {
      "react-dom": {
        root: "ReactDOM",
        commonjs2: "react-dom",
        commonjs: "react-dom",
        amd: "react-dom",
      },
    },
    {
      react: {
        root: "React",
        commonjs2: "react",
        commonjs: "react",
        amd: "react",
      },
    },
  ],
};
