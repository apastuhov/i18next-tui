import { mount, ReactWrapper } from "enzyme";
import React from "react";
import { Editor } from "../../src/toolbar/editor";

describe("Editor", () => {
  let wrapper: ReactWrapper<typeof Editor.prototype.props, {}, Editor>;
  let handleChange: (lang: string) => void;

  beforeEach(() => {
    handleChange = jest.fn();
    wrapper = mount(
      <Editor translationKey={"key"} value={"value"} onChange={handleChange} />,
    );
  });

  it("renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("should handle callback on change", () => {
    const select = wrapper.find("textarea");
    select.simulate("change", {
      target: {
        value: "value1",
      },
    });
    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith("key", "value1");
  });
});
