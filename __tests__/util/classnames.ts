import classnames from "../../src/util/classnames";

describe("classnames", () => {
  it("creates an empty string when is called without arguments", () => {
    expect(classnames()).toBe("");
  });

  it("creates class name list from hash", () => {
    expect(classnames({})).toBe("");
    expect(classnames({ hidden: undefined })).toBe("");
    expect(classnames({ hidden: true })).toBe("hidden");

    expect(classnames({
      hidden: false,
      visible: true,
    })).toBe("visible");

    expect(classnames({
      hidden: undefined,
      visible: true,
    })).toBe("visible");

    expect(classnames({
      active: true,
      hidden: false,
      visible: true,
    })).toBe("active visible");
  });

  it("creates class name list from an array", () => {
    expect(classnames([])).toBe("");
    expect(classnames([ undefined ])).toBe("");
    expect(classnames([ "hidden" ])).toBe("hidden");

    expect(classnames([
      "",
      "active",
      undefined,
      "visible",
    ])).toBe("active visible");
  });
});
