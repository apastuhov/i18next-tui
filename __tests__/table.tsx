import { mount, ReactWrapper, shallow, ShallowWrapper } from "enzyme";
import React from "react";
import { Table } from "../src/table";
import { Header } from "../src/table/header";

describe("Table", () => {
  let wrapper: ReactWrapper<typeof Table.prototype.props, {}, Table>;
  let handleSelect: (key: string) => void;

  beforeEach(() => {
    const changed = new Set();
    changed.add("key");
    handleSelect = jest.fn();
    wrapper = mount(
      <Table
        localeList={[
          {
            lang: "en",
            translations: {
              key: "Key",
              value: "Value",
            },
          },
          {
            lang: "de",
            translations: {
              key: "__STRING_NOT_TRANSLATED__",
              value: "Wert",
            },
          },
          {
            lang: "ru",
            translations: {
              key: "Ключ",
              value: "",
            },
          },
        ]}
        changed={changed}
        filterEmpty={false}
        selectedKey={"value"}
        onSelect={handleSelect}
        defaultEmptyPlaceholder={"__STRING_NOT_TRANSLATED__"}
        masterLanguage={"en"}
      />,
    );
  });

  it("renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("renders correctly when master is not specified or is not valid", () => {
    wrapper.setProps({ masterLanguage: "test" });
    expect(wrapper).toMatchSnapshot();
  });

  it("can click on table row", () => {
    const tr = wrapper.find("tbody tr").first();
    tr.simulate("click");
    expect(handleSelect).toHaveBeenCalledTimes(1);
    expect(handleSelect).toHaveBeenCalledWith("key");
  });
});

describe("Table Header", () => {
  let wrapper: ShallowWrapper<typeof Header.prototype.props, {}, Header>;

  beforeEach(() => {
    wrapper = shallow(
      <Header languageList={["en", "de"]} />,
    );
  });

  it("shouldn't update on the same props", () => {
    const result = wrapper.instance().shouldComponentUpdate({
      languageList: ["en", "de"],
    });
    expect(result).toBe(false);
  });

  it("should update on different props", () => {
    const result = wrapper.instance().shouldComponentUpdate({
      languageList: ["fr", "en"],
    });
    expect(result).toBe(true);
  });
});
