// Type definitions for i18next-tui >=1.0.1
// Project: i18next-tui
// Definitions by: Aleksey Pastuhov <aleksey.pastuhov@gmail.com>
import * as React from 'react';

export as namespace I18NextTUI;

export = I18NextTUI;

declare namespace I18NextTUI {
  export interface ITranslations {
    [key: string]: string;
  }

  export interface ILocale {
    lang: string;
    translations: ITranslations;
  }

  export interface INamespace {
    name: string;
    locales: ILocale[];
    changed?: Set<string>;
  }

  export interface IProps {
    namespaceList: INamespace[];
    defaultEmptyPlaceholder?: string;
  }

  export class I18NextTUI extends React.Component<I18NextTUI.IProps> {

  }
}
