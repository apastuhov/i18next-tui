# Changelog

## [Unreleased / TODO]

### Add

- Docker to host management tool.
- Search by key.
- Support hot-key navigation between translations.
- Add TSV export.
- Language display filter.
- Add dynamic update of translations back to application.
- Add basic statistics to analyze translations.

### Change

- Switch from sass to less/stylus.
- Think about moving to state management stuff like Redux.
- Change colors to improve accessibility.
- Improve performance (possible change of data structures and interfaces).
- Extract mocks out of test files.

### Remove

- Remove performance issues when we have more than 10,000 translations for 3 languages at once.

## [1.1.2] - Next Release

### Removed

- Remove `classnames` as dependency. There is no need in extra dependency just for few dynamic classes.

## [1.1.0] - 2018-07-30

### Added

- Provide typings for TypeScript.
- Enable demo.

## [1.0.0] - 2018-07-29

### Added

- Prevent loose of changes when a user tries to close a window or reload the page.
- Export JSON translation for a selected language.
- Support multiple name-spaces.
- Add empty cells filter.
- Highlight empty cells.
- Highlight changed rows.
- Add a field to edit translation for active language.
- Render table of all translations by languages.
- Setup CI and environment.
