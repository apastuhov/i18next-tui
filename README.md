# i18next-tui

[![pipeline status](https://gitlab.com/apastuhov/i18next-tui/badges/master/pipeline.svg)](https://gitlab.com/apastuhov/i18next-tui/commits/master)
[![coverage report](https://gitlab.com/apastuhov/i18next-tui/badges/master/coverage.svg)](https://gitlab.com/apastuhov/i18next-tui/commits/master)
[![npm version](https://badge.fury.io/js/i18next-tui.svg)](https://badge.fury.io/js/i18next-tui)

i18next Translation User Interface - is a tool to manage translation right from your web application. It is React based component.

Checkout [DEMO](https://apastuhov.gitlab.io/i18next-tui/).

Inspired by POEditor design.

**Note:** Soon I will provide docker container for those who do not like or do not use react. ;)

## Why?

1. [Standart Translation User Interface for i18next](https://github.com/i18next/i18next-webtranslate) is deprecated and too old.
2. [http://locize.com](http://locize.com) is a cool tool, but it is too complex for small teams and indie projects.
3. I did not find any FOSS stuff to manage translations from i18next.
4. That tool is not a competitor for [http://locize.com](http://locize.com).
5. It is developed just to cover basic needs in translation management.

## TOC

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Usage](#usage)
    - [Insert JSON](#insert-json)
    - [Use i18next](#use-i18next)
    - [How to generate JSON translations?](#how-to-generate-json-translations)
- [Development and contribution](#development-and-contribution)
  - [Running the tests](#running-the-tests)
  - [Coding style tests](#coding-style-tests)
- [Built With](#built-with)
- [Contributing](#contributing)
- [Versioning](#versioning)
- [Authors](#authors)
- [License](#license)

## Getting Started

These instructions will help you with the integration or contribution of that library.

### Prerequisites

Tested on React v16+ with Node v9.3.0

- react
- react-dom

### Usage

```bash
npm install -d i18next-tui
// or
yarn add i18next-tui
```

It is recommended to create protected(for dev only) route inside your app and insert component there.

```tsx
import * as I18NextTUI from "i18next-tui";

class Translations extends React.Component {
  render() {
    return <I18NextTUI.I18NextTUI
      namespaceList={namespaceList as I18NextTUI.INamespace[]}
      defaultEmptyPlaceholder="__STRING_NOT_TRANSLATED__"
    />
  }
}
```

You have 2 options to generate `namespaceList`:

- Import JSON directly. Not recommended, because it will increase the size of your bundle. But it is a fast and easy way to test.
- Use `i18next`.

#### Insert JSON

Use that approach if you have not integrated i18next or you just want to test the app. Or you have some custom dynamic wrapper on top of i18next and some dynamic translations.

```jsx
import de from "/src/locale/de/translation.json";
import en from "/src/locale/en/translation.json";

const namespaceList = [
  {
    locales: [
      {
        lang: "en",
        translations: en,
      },
      {
        lang: "de",
        translations: de,
      },
    ],
    name: "Translation",
  },
];
```

#### Use i18next

Dynamically grabs content based on translations you have loaded.

```jsx
import i18n from "i18next";

if (!i18n.isInitialized) {
  return;
}

const namespaceList = i18n.options.ns.map(namespace => {
  return {
    name: namespace,
    locales: i18n.languages.map(lng => {
      i18n.getResourceBundle(lng, namespace);
    })
  }
});
```

#### How to generate JSON translations?

If you do not know how to generate JSON files with translation from your source code. You can use `i18next-scanner`.

## Development and contribution

Install dependencies `npm install`.

**Note:** Do not use `yarn` for dev env. There is no lock file for yarn.

You can build sources with `npm run build`.

To analyze dependencies use `npm run build -- --analyze`;

To serve a demo use `npm run serve`.

```bash
npm run all
```

To simplify life - before committing you can run `all` command to build, test, update snapshots, lint.

### Running the tests

Jest is used for testing purposes. You can run tests with `npm test` or use watch mode with `npm run test-watch`.

### Coding style tests

Linters are used to prevent bad code. Use them before creating a commit.

```bash
npm run lint
```

## Built With

- Webpack
- TypeScript
- React
- Sass
- Jest
- Enzyme

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/apastuhov/i18next-tui/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/apastuhov/i18next-tui/tags).

<!--
## Authors
-->

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/apastuhov/i18next-tui/blob/master/LICENSE.md) file for details
